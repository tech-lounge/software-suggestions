# Software Suggestions


_Sourced from the Tech Lounge Discord server_


## Content


## Server

### Authentication

 - [Hanko](https://www.hanko.io/)

### Search Engines

 - [SearX](https://github.com/searx/searx)
 - [Whoogle](https://github.com/benbusby/whoogle-search)

### Virtualization
 - [KrunVM](https://github.com/containers/krunvm)

## Workstation

### Music
 
  - [Sunamu](https://github.com/NyaomiDEV/Sunamu)
